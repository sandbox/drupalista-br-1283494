<?php
/**
 * Returns the results of the commerce_product_load() for the specified product.
 *
 * @param $product_id
 *   The product ID we want to return.
 * @return
 *   product object or FALSE if not found.
 *
 * @see commerce_product_load() 
 */
function _product_resource_retrieve($product_id) {
    $product = commerce_product_load($product_id);
    
    // TODO: Gotta be tested and there are more work to be done. What work? I don't know yet.
    
    return $product;
}


/**
 * Determine whether the current user can access a product resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _product_resource_access($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}


/**
 * Creates a new product based on submitted values.
 *
 * @param $product
 *   Array representing the attributes a product edit form would submit.
 * @return
 *   TODO: Don't know yet.
 *
 * @see TODO:
 * 
 */
function _product_resource_create($product) {
    
}

/**
 * Determine whether the current user can create a product resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _product_resource_create($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}