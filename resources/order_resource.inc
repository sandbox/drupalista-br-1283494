<?php
/**
 * Returns the results of the commerce_order_load() for the specified order.
 *
 * @param $profile_id
 *   The order profile ID we want to return.
 * @return
 *   order Profile object or FALSE if not found.
 *
 * @see commerce_order_load() 
 */
function _order_resource_retrieve($order_id) {
    $order = commerce_order_load($order_id);
    
    // TODO: Gotta be tested and there are more work to be done. What work? I don't know yet.
    
    return $order;
}


/**
 * Determine whether the current user can access a order resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _order_resource_access($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}


/**
 * Creates a new order profile based on submitted values.
 *
 * @param $order
 *   Array representing the attributes a order profile edit form would submit.
 * @return
 *   TODO: Don't know yet.
 *
 * @see TODO:
 * 
 */
function _order_resource_create($order) {
    
}

/**
 * Determine whether the current user can create a order resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _order_resource_create($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}