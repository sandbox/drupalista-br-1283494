<?php
/**
 * Returns the results of the commerce_customer_profile_load() for the specified customer.
 *
 * @param $profile_id
 *   The customer profile ID we want to return.
 * @return
 *   Customer Profile object or FALSE if not found.
 *
 * @see commerce_customer_profile_load() 
 */
function _customer_resource_retrieve($profile_id) {
    $profile = commerce_customer_profile_load($profile_id);
    
    // TODO: Gotta be tested and there are more work to be done. What work? I don't know yet.
    
    return $profile;
}


/**
 * Determine whether the current user can access a customer resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _customer_resource_access($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}


/**
 * Creates a new customer profile based on submitted values.
 *
 * @param $profile
 *   Array representing the attributes a customer profile edit form would submit.
 * @return
 *   TODO: Don't know yet.
 *
 * @see commerce_customer_profile_new()
 * @see commerce_order_save()
 * 
 */
function _customer_resource_create($profile) {
    
}

/**
 * Determine whether the current user can create a customer resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _customer_resource_create($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}